# Leon Hunter



-
-
## Autobiography Overview
* Birth Details
* School Details
* Aspirations



-
### Birth details
* Age - 26
* Birth Place - Newark, DE
* Favorite Place - Glasgow, Scotland


-
### School Details
* Middle School - Gunning Bedford
* High School - William Penn
* Current Grade - N/A


-
### Aspirations
* Aspiration 1 - To make code equally ubiquitous to to mathematics and literacy
* Aspiration 2 - To significantly diversify the tech industry
* Aspiration 3 - To eliminate stigmas and cliches around disadvantaged demographics

-
-
## Cool Image
<img src = "./cool-image.jpg">
