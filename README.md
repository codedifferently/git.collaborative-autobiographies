# Code Differently Lecture Slides

## Description
* The purpose of this repository is to create a centralized location for students to describe themselves using a webpage.

## Developmental Notes
* Study the syntax of the `content.md` files to identify how to expand and modify the content.


## Viewing the application
* The application can be ran by executing the following command from the root directory of the project.
  * `python -m SimpleHTTPServer 8080`
* After running the application, navigate to `localhost:8080` from a browser to view the slides.
